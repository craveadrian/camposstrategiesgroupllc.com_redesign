<div id="section1" class="resSection">
	<div class="row">
		<h2>Strategic Solutions For Your Organization</h2>
		<p>Campos Strategic Group, LLC is a 100% minority and woman owned strategic consulting firm. We focus on helping non-profit, labor, and community organizations develop strategies for issue advocacy at the state and federal level.</p>
		<img src="public/images/content/s1img.jpg" alt="laptop">
	</div>
</div>
<div id="section2" class="resSection">
	<div class="row">
		<h1>Our Services</h1>
		<div class="flexbox">
			<dl>
				<dt> <img src="public/images/content/svcImg1.jpg" alt="Service 1"> </dt>
				<dd>Issue Advocacy & <br> Government Relations</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/svcImg2.jpg" alt="Service 2"> </dt>
				<dd>Community Outreach</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/svcImg3.jpg" alt="Service 3"> </dt>
				<dd>Strategic Fundraising</dd>
			</dl>
		</div>
	</div>
</div>
<div id="section3" class="resSection">
	<div class="row">
		<h1>Why Choose Us</h1>
		<div class="s3flex">
			<div class="choose">
				<img src="public/images/sprite.png" alt="Icon" class="bg-1">
				<p><span>Our vision</span> is to help our clients make connections between their organizational goals and the human element that makes people spur to action.</p>
			</div>
			<div class="choose">
				<img src="public/images/sprite.png" alt="Icon" class="bg-2">
				<p><span>We have many years of experience</span>  working at the grassroots level, listening & training individuals to become agents of change in their communities.</p>
			</div>
			<div class="choose">
				<img src="public/images/sprite.png" alt="Icon" class="bg-3">
				<p><span>At CSG</span>, we use people power to create organizational change. We work with our clients to develop plans that engages their membership and build their base.</p>
			</div>
			<div class="choose">
				<img src="public/images/sprite.png" alt="Icon" class="bg-4">
				<p><span>Our clients</span> include major advocacy organizations, elected officials, policy makers and labor unions. We offer creative solutions that help our clients meet their program objectives on time and on budget.</p>
			</div>
			<div class="choose">
				<img src="public/images/sprite.png" alt="Icon" class="bg-5">
				<p><span>We believe in</span> listening to our client needs and develop a plan that brings results.</p>
			</div>
		</div>
	</div>
</div>
<div id="section4">
	<div class="row">
		<div class="images">
			<img src="public/images/content/img1.jpg" alt="Image">
			<img src="public/images/content/img2.jpg" alt="Image">
			<img src="public/images/content/img3.jpg" alt="Image">
			<img src="public/images/content/img4.jpg" alt="Image">
			<img src="public/images/content/img5.jpg" alt="Image">
			<img src="public/images/content/img6.jpg" alt="Image">
		</div>
	</div>
</div>
<div id="section5" class="resSection">
	<div class="row">
		<div id="content">
			<div class="row">
				<h1>Contact Us</h1>
				<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
					<div class="form-top">
						<label><span class="ctc-hide">Name</span>
							<input type="text" name="name" placeholder="Name:">
						</label>
						<!-- <label><span class="ctc-hide">Address</span>
							<input type="text" name="address" placeholder="Address:">
						</label> -->
						<label><span class="ctc-hide">Phone</span>
							<input type="text" name="phone" placeholder="Phone:">
						</label>
						<label><span class="ctc-hide">Email</span>
							<input type="text" name="email" placeholder="Email:">
						</label>
					</div>
					<label><span class="ctc-hide">Message</span>
						<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
					</label>
					<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
					<div class="g-recaptcha"></div>
					<label>
						<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
					</label>
					<?php endif ?>
					<button type="submit" class="ctcBtn" disabled>SUBMIT FORM</button>
				</form>
			</div>
		</div>
	</div>
</div>
<div id="section6" resSection>
	<div class="row">
		<p>We have expertise in organizational & grassroots leadership development, event creation and management, national and international campaigns, & grassroots issue advocacy.</p>
	</div>
</div>
