<div id="content">
  <div class="row">
    <h1>Services</h1>
    <div class="inner-services">
      <h2>Campos Strategies Group, LLC</h2>
      <ol>
        <li><p><strong>Issue Advocacy and Government Relations:</strong> At CSG we specialize in helping organizations develop strategies to educate and engage their members around issues that matter to them. We work with you to personalize your message, design an education plan for your members and community, build strategic alliances/coalitions, and prepare a media outreach plan to ensure that your voice gets heard by policy makers at the state and federal level. With more than 15 years of experience in advocacy and government relations, CSG can help your organization create the impact you desire by engaging your members at the grassroots level. Our relationships with State and Federal elected officials, Cabinet members, community leaders and senior policy staffers ensures that your position is heard by those in elected office. Our background on legislative campaigning gives us an edge in advancing your position with the right people at the right moment.</p></li>

        <li><p><strong>Community Outreach:</strong> CSG has more than 15 years of experience working with community groups to advance issues and create results. We bring our knowledge of the communities we work with to help clients advance their objectives. By understanding your goals, we work with you to bring your message to the community and help you build relationships that will broaden your base of support. We have strong relationships with the Latino, African American, women, youth and LGBT communities as well as the religious and evangelicals groups. Our strategic outreach efforts ensure that our clients objectives are met and amplified.</p></li>

        <li><p><strong>Strategic Fundraising:</strong> CSG works with your organization and or campaign to design a strategic fundraising plan that expands your base of support. We work to further engage your base, to identify new areas of fundraising, and identify new strategies for expanding your level of contributions by current donors. We work with companies, foundations and government agencies to create synergy between their philanthropic goals and the needs of local communities.</p></li>
      </ol>
    </div>
  </div>
</div>
